﻿using System;

namespace FunctiiStructurideControl
{
    internal class Program
    {
        /*
         * Introducere metode / functii
         * Structuri de control alternative
         */
        static void Main(string[] args)
        {
            AfiseazaNume();

            // parametrii
            AfiseazaPatrat(5);

            // 2 paramentrii
            Console.Write("Nume: ");
            String nume = Console.ReadLine();

            Console.Write("Prenume: ");
            String prenume = Console.ReadLine();
            AfiseazaNumeComplet(nume, prenume);

            // valori returnate
            int suma = CalcSuma(3, 4);
            Console.WriteLine("Suma: " + suma);

            // pasarea argumentelor prin referinta
            ConcateneazaNume(ref nume, prenume);
            Console.WriteLine("Verificare nume:" + nume);

            // argumentul out
            TransformaNumeStudent(nume, prenume, out String numeComplet);
            Console.WriteLine(numeComplet);

            // structura if
            Console.WriteLine("Intorduceti varsta dvs: ");
            bool rezultatConversie = int.TryParse(Console.ReadLine(), out int v1);

            Console.WriteLine("Rezultat conversie: " + rezultatConversie);
            Console.WriteLine("Tip variabila: " + v1.GetType());
            Console.WriteLine("Valoare variabila: " + v1);

            if (v1 >= 18)
            {
                Console.WriteLine("Ai dreptul sa conduci.");
            }
            else
            {
                Console.WriteLine("Mai asteapta");
            }


            // if prescurtat - cond ? adev : false
            Console.WriteLine(v1 >= 18 ? "Go ahead" : "Mai asteapta");

            // optimizare 1
            int v2 = DeterminaVarsta();

            if (v2 >= 18)
                Console.WriteLine("Ai dreptul sa conduci.");
            else
                Console.WriteLine("Mai asteapta");

            // optimizare 3 - generalizam functia
            int v3 = CereNrIntreg("Introduceti varsta dvs: ");
            Console.WriteLine("Numar introdus: " + v3);

            // folosim metoda declarata mai sus
            int v4 = CereNrIntreg("Introduceti nr norocos: ");
            Console.WriteLine("Nr norocos: " + v4);

            // Switch
            int carteDeJoc = CereNrIntreg("Introduceti o carte de joc preferata (0-13): ");
            string denumireCarte;
            switch (carteDeJoc)
            {
                case 13:
                    denumireCarte = "Popa";
                    break;

                case 12:
                    denumireCarte = "Dama";
                    break;

                case 11:
                    denumireCarte = "Valet";
                    break;

                case 1:
                    denumireCarte = "AS";
                    break;

                case 0:
                    denumireCarte = "Joker";
                    break;

                default:
                    denumireCarte = carteDeJoc.ToString();
                    break;
            }

            Console.WriteLine("Cartea de joc aleasa: " + denumireCarte);

            Console.ReadKey();
        }

        static void AfiseazaNume()
        {
            Console.WriteLine("Numele meu este: Marian");
        }

        static void AfiseazaPatrat(int numar)
        {
            Console.WriteLine($"Patratul lui {numar} este: {numar * numar}");
        }

        static void AfiseazaNumeComplet(String nume, String prenume)
        {
            Console.WriteLine(nume + " " + prenume);
            // Console.WriteLine($"{nume} {prenume}"); // sau putem folosi interpolare
        }

        static void ConcateneazaNume(ref String nume, String prenume)
        {
            nume = nume.ToUpper();
            Console.WriteLine($"{nume} {prenume}");
        }

        private static void TransformaNumeStudent(string nume, string prenume, out String numeComplet)
        {
            numeComplet = $"{nume} {prenume}";
        }

        static int DeterminaVarsta()
        {
            Console.WriteLine("Intorduceti varsta dvs: ");
            int.TryParse(Console.ReadLine(), out int v1);

            return v1;
        }

        static int CereNrIntreg(string mesaj = "Introduceti un numar:")
        {
            Console.WriteLine(mesaj.ToUpper());

            int.TryParse(Console.ReadLine(), out int numar);

            return numar;
        }

        static int CalcSuma(int a, int b)
        {
            return a + b;
        }
    }
}